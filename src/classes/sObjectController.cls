public class sObjectController {
    
    
    public list<string> fieldApiList {get; set;}
    public string selectedObjectApi {get; set;}
    public map<String, Schema.SObjectType> GlobalSChemaMap = Schema.getGlobalDescribe();
    map<string, String> sObjectApi_Map = new map<string, String>();
    
    
    public sObjectController() {
        
        selectedObjectApi = '';
        fieldApiList  = new list<string>();
        for (Schema.SObjectType Obj : GlobalSChemaMap.values()) {
            
            Schema.DescribeSObjectResult ObjDesc = Obj.getDescribe();
            if(objDesc.isQueryable() && objDesc.isSearchable() && objDesc.isCreateable()) {
                fieldApiList.add(ObjDesc.getName());
                sObjectApi_Map.put(ObjDesc.getName(),ObjDesc.getLabel());
            }
        }
        fieldApiList.sort();
    }
    
    
    public List<SelectOption> getfieldApi() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        for(string str : fieldApiList) {
            
            options.add(new SelectOption(str, sObjectApi_Map.get(str)));
        }
        return options;
    }
    
    
    public PageReference saveSobjectMetadata() {
        system.debug(GlobalSChemaMap.keySet());  //sObjectApi_Map.get(selectedObjectApi)
        String[] str = new String[]{selectedObjectApi};
        Schema.SObjectType sobjTypeVal = GlobalSChemaMap.get(selectedObjectApi);
        
       Schema.DescribeSobjectResult[] r = Schema.describeSObjects(str);

        //Schema.DescribeSObjectResult r = sobjTypeVal.sObjectType.getDescribe();
		//String keyPrefix = r[0].getKeyPrefix();
		//System.debug('Printing --'+keyPrefix );
        CustomMetadataService.createMetadata(r[0], null);
        return null;
    }   
    
}