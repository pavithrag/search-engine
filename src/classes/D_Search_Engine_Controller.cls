/*
*
*Author       : Team ET Snipers
*Description  : Class for Search Engine Objects Customization Controller
*Created Date : 12/09/15
*Version      : 1.0
*
*/
public class D_Search_Engine_Controller {

	public Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	public List <SelectOption> objectNames{public get; private set;}
    public String selectedObject {get; set;}
    public list<SobjectWrapperCls> listOfsObjects {get;set;}


	public D_Search_Engine_Controller(){

		//objectNames = initObjNames();
		//initObjNames();
	}

	// Populate SelectOption list  - 
    // find all sObjects available in the organization
    @RemoteAction
    public static list<SobjectWrapperCls> initObjNames() {
        
        list<SobjectWrapperCls> sObjectWrapList = new list<SobjectWrapperCls>();
        List<String> entities = new List<String>();
        Map<string, string> sObjectMap = new Map<string, string>();
        Map<string, string> sObjectApi_Map = new Map<string, string>();
        for(D_sObject_Setting__mdt sobj : [SELECT DeveloperName,Label,MasterLabel,QualifiedApiName FROM D_sObject_Setting__mdt]) {
        	sObjectMap.put(sobj.DeveloperName, sobj.label);
        }
 		Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();

		for (Schema.SObjectType Obj : schemaMap.values()) {		            
		            Schema.DescribeSObjectResult ObjDesc = Obj.getDescribe();
		            if(objDesc.isQueryable() && objDesc.isSearchable() && objDesc.isCreateable()) {
		                entities.add(ObjDesc.getName());
		                sObjectApi_Map.put(ObjDesc.getName(),ObjDesc.getLabel());		                
		        }
		}

        for(string str : entities) {
        	boolean bool = sObjectMap.containsKey(str) ? true : false;
         	sObjectWrapList.add(new SobjectWrapperCls(bool,str,sObjectApi_Map.get(str)));
        }
        return sObjectWrapList;
   }

    public class SobjectWrapperCls{
        public boolean isChecked;
        public string sobjectAPI;
        public string sobjectLabel;
        public SobjectWrapperCls(boolean isChecked, string sobjectAPI, string sobjectLabel){
        	this.isChecked = isChecked;
        	this.sobjectAPI = sobjectAPI;
        	this.sobjectLabel = sobjectLabel;
        }
  }
  
  		/*
        * Submit the response
        */
        @RemoteAction
        public static void submitListOfResponse(List<String> allObjectAPI){          
            try{
             list<Schema.DescribeSobjectResult> sObjectResult = new list<Schema.DescribeSobjectResult>();
             List<Schema.DescribeSobjectResult> sObjectUncheckResult = new List<Schema.DescribeSobjectResult>();
             Map<String,String> sObjectMap = new Map<String,String>();
             List<String> allObjectName = new List<String>();
             Set<String> objectType = new Set<String>(); 
             
             for(D_sObject_Setting__mdt sobj : [SELECT DeveloperName,Label,MasterLabel,QualifiedApiName FROM D_sObject_Setting__mdt]) {
         					sObjectMap.put(sobj.DeveloperName, sobj.label);
         					allObjectName.add(sobj.DeveloperName);
        	  }
        	  
        	//----- Iterate to list to get the list of check Objects  
             for(integer i=0; i<allObjectAPI.size();i++){
             	if(!sObjectMap.containsKey(allObjectAPI[i])){
	             	 System.debug('ObjectApi '+allObjectAPI[i]);
		             Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
		             Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(allObjectAPI[i]);	   
			    	 Schema.DescribeSObjectResult describeSObjectResultObj = SObjectTypeObj.getDescribe();
			    	 sObjectResult.add(describeSObjectResultObj);
             	}        
             }               
             System.debug('sObjectResult '+sObjectResult);	 
             CustomMetadataService.createMetadata(sObjectResult, null);  
                                 
              for(String allAPI : allObjectAPI){
                	objectType.add(allAPI);
                }
             
             //----- Iterate to list to get the list of unchecked Objects
             for(integer i=0; i<allObjectName.size();i++){
             		if(!objectType.contains(allObjectName[i])){             			
             			System.debug('ObjectApi '+allObjectName[i]);
		             	Map<String, Schema.SObjectType> describeMap = Schema.getGlobalDescribe(); 
		             	Schema.SObjectType sObjectType = describeMap.get(allObjectName[i]);	   
			    	 	Schema.DescribeSObjectResult describeSObjectRes = sObjectType.getDescribe();            			
             			sObjectUncheckResult.add(describeSObjectRes);
             		}
             }                        
             System.debug('sObjectUncheckResult '+sObjectUncheckResult);
             CustomMetadataService.deleteMetadata(sObjectUncheckResult, null);
                
            }catch(Exception e){
                System.debug('Exception '+e.getMessage());
            }
                
        }

		/*
		* Get all Objects that are checked
		*/
		@RemoteAction
    public static List<sObjectFields> allObjectList() {
    	List<sObjectFields> sObjectWrapList = new List<sObjectFields>();
    	Map<string, string> sObjectMap = new Map<string, string>();
    	List<String> sObjects = new List<String>();
    	
    	for(D_sObject_Setting__mdt sobj : [SELECT DeveloperName,Label,MasterLabel,QualifiedApiName FROM D_sObject_Setting__mdt]) {
        	sObjectMap.put(sobj.DeveloperName, sobj.label);
        	sObjects.add(sobj.DeveloperName);
        }
        
        
        for(String apiName : sObjects){
        	SObjectType objectType = Schema.getGlobalDescribe().get(apiName);
			Map<String,Schema.SObjectField> mfields = objectType.getDescribe().fields.getMap();
			System.debug('Fields '+mfields);
			List<String> objectFields = new List<String>();
			for(String allField: mfields.keySet()){
					if ((allField != 'Name') && (mfields.get(allField).getDescribe().isAccessible())) {
                			objectFields.add(allField);
            		}									
			}
        	sObjectFields allObjectFields = new sObjectFields(apiName,sObjectMap.get(apiName),null);
  			sObjectWrapList.add(allObjectFields);
        }
    	    	
    	return sObjectWrapList;
    }
    
    
	//----- Wrapper to get all objects with fields	
    public class sObjectFields{
        public string sobjectAPI;
        public string sobjectLabel;
        public List<String> sobjectField;
        public sObjectFields(string sobjectAPI, string sobjectLabel,List<String> sobjectField){        	
        	this.sobjectAPI = sobjectAPI;
        	this.sobjectLabel = sobjectLabel;
        	this.sobjectField = sobjectField;
        }
  }
  
  
}