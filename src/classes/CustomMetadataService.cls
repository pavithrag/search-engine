/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
 * Wraps the Apex Metadata API to provide create, update and delete operations around Custom Metadata SObject's
 *
 *  NOTE: Upsert is currently not supported by the Metadata API
 *
 * TODO: Support bulk requests
 * TODO: Support All Or Nothing (new for Metadata API v34.0)
 **/
public class CustomMetadataService {

    /**
     * Insert the given Custom Metadata records into the orgs config
     **/
    public static void createMetadata(list<Schema.DescribeSobjectResult> records, list<Schema.DescribeFieldResult> fieldRes) {
    	

        // Call Metadata API and handle response
        MetadataService.MetadataPort service = createService();
        if(fieldRes == null) {
        for(Schema.DescribeSobjectResult describeSobjRes : records) {
        List<MetadataService.SaveResult> results = service.createMetadata(new List<MetadataService.Metadata> { toCustomMetadata(describeSobjRes, fieldRes[0]) });
        handleSaveResults(results[0]);
        }
        } else {
        	for(Schema.DescribeFieldResult describeFieldRes : fieldRes) {
        List<MetadataService.SaveResult> results = service.createMetadata(new List<MetadataService.Metadata> { toCustomMetadata(records[0], describeFieldRes) });
        handleSaveResults(results[0]);
        }
        }
    }

    /**
     * Update the given Custom Metadata records in the orgs config
     **/
    public static void updateMetadata(list<Schema.DescribeSobjectResult> records, list<Schema.DescribeFieldResult> fieldRes) {
        // Call Metadata API and handle response
        MetadataService.MetadataPort service = createService();
        if(fieldRes == null) {
        for(Schema.DescribeSobjectResult describeSobjRes : records) {
        List<MetadataService.SaveResult> results = service.updateMetadata(new List<MetadataService.Metadata> { toCustomMetadata(describeSobjRes,fieldRes[0]) });
        handleSaveResults(results[0]);
        }
    } else {
    	for(Schema.DescribeFieldResult dfr : fieldRes) {
    	List<MetadataService.SaveResult> results = service.createMetadata(new List<MetadataService.Metadata> { toCustomMetadata(records[0], dfr) });
        handleSaveResults(results[0]);
    	}
    }
    }

    /**
     * Delete the given Custom Metadata records from the orgs config
     **/
    public static void deleteMetadata(list<Schema.DescribeSobjectResult> records, list<Schema.DescribeFieldResult> fieldRes) {

        MetadataService.MetadataPort service = createService();
        List<String> qualifiedFullNames = new List<String>();
        if(fieldRes == null) {
        for(Schema.DescribeSobjectResult customMetadataFullName : records) {
            qualifiedFullNames.add('D_sObject_Setting__mdt.'+customMetadataFullName.getName());
                //qualifiedMetadataType.getDescribe().getName() + '.' + customMetadataFullName);
			system.debug(qualifiedFullNames+'******');
        }
        List<MetadataService.DeleteResult> results = service.deleteMetadata('CustomMetadata', qualifiedFullNames);    
        handleDeleteResults(results[0]);
    } else {
    	for(Schema.DescribeFieldResult customMetadataFullName : fieldRes) {
            qualifiedFullNames.add('D_sObjectField_Setting__mdt.'+customMetadataFullName.getName());
                //qualifiedMetadataType.getDescribe().getName() + '.' + customMetadataFullName);
			system.debug(qualifiedFullNames+'******');
        }
    	List<MetadataService.DeleteResult> results = service.deleteMetadata('CustomMetadata', qualifiedFullNames);    
        handleDeleteResults(results[0]);
    }
    }

    public class CustomMetadataServiceException extends Exception {}

    /**
     * Takes the SObject instance of the Custom Metadata Type and translates to a Metadata API Custmo Metadata Type
     **/
    private static MetadataService.CustomMetadata toCustomMetadata(Schema.DescribeSobjectResult customMetadataRecord, Schema.DescribeFieldResult fieldRes) {
        
        list<MetadataService.CustomMetadata> cmList = new list<MetadataService.CustomMetadata>();
        MetadataService.CustomMetadata cmIns;
        if(fieldRes == null) {
        
        cmIns = new MetadataService.CustomMetadata();
        SObjectType recordType = customMetadataRecord.getSObjectType();
        cmIns.fullName = 'D_sObject_Setting__mdt.'+recordType.getDescribe().getName();
        //recordType.getDescribe().getName().replace('__c', '__mdt') +'.' +'Name';
        //D_sObject_Setting__mdt.
        //recordType customMetadataRecord.get('DeveloperName');
        cmIns.label = customMetadataRecord.getLabel();
        //cmList.add(cmIns);
        } else {
        	
        	cmIns = new MetadataService.CustomMetadata();
        SObjectType recordType = customMetadataRecord.getSObjectType();
        cmIns.fullName = 'D_sObject_Setting__mdt.'+recordType.getDescribe().getName();
        cmIns.label = customMetadataRecord.getLabel();
        	//for(Schema.DescribeFieldResult dfr : fieldRes) {
        	MetadataService.CustomMetadataValue cmdv = new MetadataService.CustomMetadataValue();
            cmdv.field = 'D_Field_Label__c';
            cmdv.value = fieldRes.getLabel(); // TODO: More work here, type conversion
            cmIns.values.add(cmdv);
        MetadataService.CustomMetadataValue cmdv1 = new MetadataService.CustomMetadataValue();
		cmdv1.field = 'D_Field_Name__c';
        cmdv1.value = fieldRes.getName(); // TODO: More work here, type conversion
        cmIns.values.add(cmdv1);
        	//}
       // List<MetadataService.SaveResult> results = 
           // service.createMetadata(new List<MetadataService.Metadata> { cm });
        //handleSaveResults(results[0]); //toCustomMetadata(records[0])
        	
        	
        }
        
        
       /* for(SObjectField sObjectField : recordType.getDescribe().fields.getMap().values()) {
            DescribeFieldResult dsr = sObjectField.getDescribe();
            if(!dsr.isCustom())
                continue;
            Object fieldValue = customMetadataRecord.get(sObjectField);
            if(fieldValue == null)
                continue;
            MetadataService.CustomMetadataValue cmdv = new MetadataService.CustomMetadataValue();
            cmdv.field = dsr.getName();
            cmdv.value = fieldValue+''; // TODO: More work here, type conversion
            cm.values.add(cmdv);
        } */
        return cmIns;
    }

    /** 
     * Connect to the Metadata API 
     **/
    private static MetadataService.MetadataPort createService()
    { 
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;     
    }

    /**
     * Example helper method to interpret a SaveResult, throws an exception if errors are found
     **/
    private static void handleSaveResults(MetadataService.SaveResult saveResult)
    {
        // Nothing to see?
        if(saveResult==null || saveResult.success)
            return;
        // Construct error message and throw an exception
        if(saveResult.errors!=null) 
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') + 
                    'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' + 
                    ( error.fields!=null && error.fields.size()>0 ? 
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                throw new CustomMetadataServiceException(String.join(messages, ' '));
        }
        if(!saveResult.success)
            throw new CustomMetadataServiceException('Request failed with no specified error.');
    }   

    /**
     * Example helper method to interpret a SaveResult, throws an exception if errors are found
     **/
    private static void handleDeleteResults(MetadataService.DeleteResult deleteResult)
    {
        // Nothing to see?
        if(deleteResult==null || deleteResult.success)
            return;
        // Construct error message and throw an exception
        if(deleteResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (deleteResult.errors.size()==1 ? 'Error ' : 'Errors ') + 
                    'occured processing component ' + deleteResult.fullName + '.');
            for(MetadataService.Error error : deleteResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' + 
                    ( error.fields!=null && error.fields.size()>0 ? 
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                throw new CustomMetadataServiceException(String.join(messages, ' '));
        }
        if(!deleteResult.success)
            throw new CustomMetadataServiceException('Request failed with no specified error.');        
    }   
}